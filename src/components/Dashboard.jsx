// src/components/Dashboard.js

import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import AgGrid from './AgGrid';
import Charts from './Chart';


const Dashboard = () => {
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const [missionData, setMissionData] = useState([]);

  useEffect(() => {
    if (isLoggedIn) {
      
      fetch('https://www.ag-grid.com/example-assets/space-mission-data.json')
        .then((response) => response.json())
        .then((data) => setMissionData(data))
        .catch((error) => console.error('Error fetching data:', error));
    }
  }, [isLoggedIn]);

  if (!isLoggedIn) {
    return <div>Please log in again.</div>;
  }

  return (
    <div>
      <h2>SpaceVue-Dashboard</h2>
      <AgGrid missionData={missionData} />
      <Charts missionData={missionData} />
    </div>
  );
};

export default Dashboard;
