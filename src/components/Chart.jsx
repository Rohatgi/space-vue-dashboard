
import '@ag-grid-community/all-modules/dist/styles/ag-grid.css';
import '@ag-grid-community/all-modules/dist/styles/ag-theme-alpine.css';

import React from 'react';
import { AgChartsReact } from 'ag-charts-react';

const Charts = ({ missionData }) => {
  const successfulMissions = missionData.filter((mission) => mission.successful);
  const unsuccessfulMissions = missionData.filter((mission) => !mission.successful);

  const chartData = [
    { category: 'Successful Missions', value: successfulMissions.length },
    { category: 'Unsuccessful Missions', value: unsuccessfulMissions.length },
  ];

  const options = {
    data: chartData,
    series: [{
      type: 'pie',
      angleKey: 'value',
      labelKey: 'category',
    }],
  };

  return (
    <div>
      <h3>Mission Outcome</h3>
      <AgChartsReact options={options} />
    </div>
  );
};

export default Charts;
