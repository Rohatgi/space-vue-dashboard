
import React from 'react';

export const AgChartsReact = ({ options }) => {
  // Mocked behavior, you can customize it as needed for your tests
  return <div data-testid="mock-ag-charts">{JSON.stringify(options)}</div>;
};
