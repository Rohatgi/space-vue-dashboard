// AgGrid.test.js
import React from 'react';
import { render, screen } from '@testing-library/react';
import AgGrid from '../components/AgGrid';

const mockMissionData = [
  {
    mission: 'Test Mission',
    company: 'Test Company',
    location: 'Test Location',
    date: '2022-01-01',
    time: '12:00:00',
    rocket: 'Test Rocket',
    price: 1000000,
    successful: true,
  },
 
];

describe('AgGrid Component', () => {
  it('renders AgGrid component with provided mission data', () => {
    render(<AgGrid missionData={mockMissionData} />);
    const missionDataHeader = screen.getByText('Mission Data');
    expect(missionDataHeader).toBeInTheDocument();

    // Add more assertions based on your component's structure
  });

  it('displays mission data correctly in the table', () => {
    render(<AgGrid missionData={mockMissionData} />);

    
    expect(screen.getByText('Test Mission')).toBeInTheDocument();
    expect(screen.getByText('Test Company')).toBeInTheDocument();
    
  });

 

  it('handles pagination correctly', () => {
    render(<AgGrid missionData={mockMissionData} />);
    
  });

  it('handles sorting correctly', () => {
    render(<AgGrid missionData={mockMissionData} />);
   
  });

  
});
