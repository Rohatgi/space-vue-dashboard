// src/__tests__/LoginPage.test.js

import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import LoginPage from '../components/LoginPage';


// Mock the store with a simplified state
const mockStore = configureStore({
  reducer: {
    auth: {
      isLoggedIn: false,
    },
  },
});

jest.mock('../redux/actions/authActions', () => ({
  login: jest.fn(),
}));

// Add an error boundary component
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can log the error to an error reporting service here
    console.error('Error caught by error boundary:', error, errorInfo);
  }

  render() {
    return this.state.hasError ? <div>Error occurred!</div> : this.props.children;
  }
}

test('renders login page', () => {
    render(
      <Router>
        <Provider store={mockStore}>
        <ErrorBoundary>
          <LoginPage />
          </ErrorBoundary>
        </Provider>
        
      </Router>
    );
  
  });



test('handles login button click', async () => {
    render(
      <Router>
        <Provider store={mockStore}>
          <ErrorBoundary>
            <LoginPage />
          </ErrorBoundary>
        </Provider>
      </Router>
    );
  
    
  });
  
