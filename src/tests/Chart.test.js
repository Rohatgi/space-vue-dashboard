// Charts.test.js
import React from 'react';
import { render, screen } from '@testing-library/react';
import Charts from '../components/Chart';

const mockMissionData = [
  {
    mission: 'Successful Mission',
    successful: true,
  },
  {
    mission: 'Unsuccessful Mission',
    successful: false,
  }
];

describe('Charts Component', () => {
  it('renders Charts component with provided mission data', () => {
    render(<Charts missionData={mockMissionData} />);
    const missionOutcomeHeader = screen.getByText('Mission Outcome');
    expect(missionOutcomeHeader).toBeInTheDocument();

  
  });

  it('displays pie chart with correct data', () => {
    render(<Charts missionData={mockMissionData} />);
    const successfulMissionsLabel = screen.getByText('Successful Missions');
    const unsuccessfulMissionsLabel = screen.getByText('Unsuccessful Missions');

    expect(successfulMissionsLabel).toBeInTheDocument();
    expect(unsuccessfulMissionsLabel).toBeInTheDocument();

    
  });


});
