// Dashboard.test.js
import React from 'react';
import { render, screen } from '@testing-library/react';
import Dashboard from '../components/Dashboard';

// Add an error boundary component
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can log the error to an error reporting service here
    console.error('Error caught by error boundary:', error, errorInfo);
  }

  render() {
    return this.state.hasError ? <div>Error occurred!</div> : this.props.children;
  }
}

test('renders dashboard page', () => {
  render(<ErrorBoundary>
    <Dashboard />
    </ErrorBoundary>);
  const dashboardElement = screen.getByText('SpaceVue-Dashboard');
  expect(dashboardElement).toBeInTheDocument();
});
